import { Component } from '@angular/core';
import { Calculadora } from '../models/Calculadora';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  calculadora: Calculadora; //motor
  resp: number;
  constructor() {
    this.calculadora = new Calculadora(); //lataria
  }

  //fios sla tbm 
  private calcular(operacao: string){
    this.calculadora.operacao = operacao;
    this.resp = this.calculadora.calcular();
  }
}
